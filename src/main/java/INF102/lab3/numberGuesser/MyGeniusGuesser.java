package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int lowerBound = number.getLowerbound();
        int upperBound = number.getUpperbound();

        while (true) {
            int guess = (lowerBound + upperBound) / 2;
            int result = number.guess(guess);

            if (result == 0) {
                return guess; // Found the number
            } else if (result == -1) {
                lowerBound = guess + 1; // Adjust lower bound
            } else {
                upperBound = guess - 1; // Adjust upper bound
            }
        }
    }

}
