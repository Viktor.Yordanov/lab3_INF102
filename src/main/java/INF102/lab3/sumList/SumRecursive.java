package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        return sumRecursiveHelper(list, 0);
    }

    private long sumRecursiveHelper(List<Long> list, int index) {
        if (index == list.size()) {
            return 0; // Base case: if we've reached the end of the list, the sum is 0.
        } else {
            long currentElement = list.get(index); // Get the current element.
            long sumOfRest = sumRecursiveHelper(list, index + 1); // Recursively calculate sum of the rest of the list.
            return currentElement + sumOfRest; // Add the current element to the sum of the rest.
        }
    }
    

}
