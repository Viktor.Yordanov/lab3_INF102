package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        return findPeak(numbers, 0, numbers.size() - 1);
    }

    private int findPeak(List<Integer> numbers, int left, int right) {
        int mid = left + (right - left) / 2;

        if ((mid == 0 || numbers.get(mid) >= numbers.get(mid - 1)) &&
                (mid == numbers.size() - 1 || numbers.get(mid) >= numbers.get(mid + 1))) {
            return numbers.get(mid); // Found a peak element
        } else if (mid > 0 && numbers.get(mid - 1) > numbers.get(mid)) {
            // Go left
            return findPeak(numbers, left, mid - 1);
        } else {
            // Go right
            return findPeak(numbers, mid + 1, right);
        }
    }
}


